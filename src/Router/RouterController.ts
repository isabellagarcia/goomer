import express from 'express';
import RouterResponse from './RouterResponse';
import RestaurantsController from '../Controllers/RestaurantsController';
import ProductsController from '../Controllers/ProductsController';

export default class RouterController{

    public routes: express.Router;

    constructor(){
        this.routes = express.Router();
        this.middlewares();
    }

    public middlewares(){

        this.loadRoutes();

        this.routes.use('*', (req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.notFound(res));

        this.routes.use((error: Error, req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.serverError(error, res));
    }

    private loadRoutes(){
        this.routes.get('/restaurants', (req, res) => RestaurantsController.restaurants(req, res));
        this.routes.get('/restaurant/:id', (req, res) => RestaurantsController.restaurantById(req, res));
        this.routes.post('/restaurant/:id', (req, res) => RestaurantsController.restaurant(req, res));
        this.routes.delete('/restaurant/:id', (req, res) => RestaurantsController.deleteRestaurant(req, res));
        this.routes.post('/restaurant', (req, res) => RestaurantsController.restaurant(req, res));

        this.routes.get('/:id/products', (req, res) => ProductsController.productsByRestaurant(req, res));
        this.routes.post('/product/:id', (req, res) => ProductsController.product(req, res));
        this.routes.post('/product', (req, res) => ProductsController.product(req, res));
        this.routes.delete('/product/:id', (req, res) => ProductsController.deleteProduct(req, res));
    }
}