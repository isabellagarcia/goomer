import express from 'express';
import http from 'http';
import RouterController from './Router/RouterController';
import bodyParser from 'body-parser';
import compression from 'compression';
import * as firebaseAdmin from "firebase-admin";


const app: express.Express = express();
const server: http.Server = http.createServer(app);
const port: number = 4000;
const router: RouterController = new RouterController;


app.use(bodyParser.json()); // converte body para objeto
app.use(compression()); // compressão de GZip 
app.use(router.routes); // importa rotas do express
firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert('./goomer-lista-rango-firebase.json'),
    databaseURL: 'https://goomer-lista-rango.firebaseio.com'
  });

server.listen(port, () => {
    console.log('Server start');
})
