import express from 'express';
import RouterResponse from '../Router/RouterResponse';
import * as firebaseAdmin from "firebase-admin";
import { DocumentData } from '@google-cloud/firestore';


export default class ProductsController {

    /**
     * productsByRestaurant
     *
     * Retorna todos os produtos de um restaurante
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static productsByRestaurant(req: express.Request, res: express.Response){
        let id: any = req.params.id;
        const db = firebaseAdmin.firestore();
        var products:  Array<any> = [];

        db.collection('products').where('restaurant', '==', id).get().then((snapshot) => {
            snapshot.forEach((doc) => {
                let data = doc.data();

                let product = {
                    ID: data.id,
                    Nome: data.name,
                    Categoria: data.category,
                    Preço: data.price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}),
                    Promoção: {
                        Descrição:  data.sale.description,
                        Preço: data.sale.price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}),
                        Data: `${data.sale.date.week == 'weekend' ? 'Fim de semana,' : 'Dia da semana,'}` + ' das '  + data.sale.date.start_hour + ' às ' + data.sale.date.final_time
                    }
                }

                products.push(product);
            });

            RouterResponse.success({data: products}, res);
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao buscar produtos do restaurante', res);
        });
        
    }

    /**
     * product
     *
     *  Adiciona ou atualiza os dados de um produtoatravés do ID
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static product(req: express.Request, res: express.Response){
        let id: any = req.params.id;
        let data: any = req.body;

        const db = firebaseAdmin.firestore().collection('products');

        var product = {
            name: data.name,
            price: +data.price,
            category: data.category,
            restaurant: data.restaurant
        }

        if(data['sale']){
            product['sale'] = {
                description: data.sale.description,
                price: +data.sale.price,
                date: {
                    final_time: data.sale.date.final_time,
                    start_hour: data.sale.date.start_hour,
                    week: data.sale.date.week
                }
            }
        }

        if(id){
            db.doc(id).set(product, {merge: true}).then((result) => {
                RouterResponse.success({data: 'Produto alterado com sucesso.'}, res);
            }).catch((err) => {
                RouterResponse.error('Houve um erro ao atualizar o produto.', res);
            });
        }else{
            db.add(product).then((document) => {
                db.doc(document.id).set({id: document.id}, {merge: true});
                RouterResponse.success({data: 'Produto adicionado com sucesso.'}, res);
            }).catch((err) => {
                RouterResponse.error('Houve um erro ao adicionar o produto.', res);
            });

        }
        
    }

    /**
     * deleteProduct
     *
     * Excluí um produto pelo ID
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static deleteProduct(req: express.Request, res: express.Response){
        let id: any = req.params.id;

        const db = firebaseAdmin.firestore();

        db.collection('products').doc(id).delete().then((result) => {
            RouterResponse.success({data: 'Produto excluído com sucesso.'}, res);
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao excluir o produto.', res);
        });
    }

}