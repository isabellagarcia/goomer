import express from 'express';
import RouterResponse from '../Router/RouterResponse';
import * as firebaseAdmin from "firebase-admin";
import { DocumentData } from '@google-cloud/firestore';


export default class RestaurantsController {

    /**
     * restaurants
     *
     * Listar todos os restaurantes
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static restaurants(req: express.Request, res: express.Response){
        const db = firebaseAdmin.firestore();
        var restaurantsList: Array<any> = [];

        db.collection('restaurants').get().then((snapshot) => {
            snapshot.forEach((doc) => {
                let data = doc.data();
                let restaurant = {
                    ID: data.id,
                    Nome: data.name,
                    Endereço: data.address,
                    "Horário de Funcionamento": {
                        "De segunda à sexta": "Das " + data.opening_hours.weekday.start_time + " às " + data.opening_hours.weekday.final_hour,
                        "Fim de semana": "Das " + data.opening_hours.weekend.start_time + " às " + data.opening_hours.weekend.final_hour
                    }
                };
                restaurantsList.push(restaurant);
            });

            RouterResponse.success({data: restaurantsList}, res);
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao buscar restaurantes', res);
        });
    }

    /**
     * restaurantById
     *
     * Retorna os dados de um restaurante pelo id
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static restaurantById(req: express.Request, res: express.Response){
        let id: any = req.params.id;
        const db = firebaseAdmin.firestore();

        db.collection('restaurants').doc(id).get().then((document) => {
            if(!document.data()){
                RouterResponse.error('Não foi encontrado restaurante com este ID.', res);
            }
            let data = document.data();
            let restaurant = {
                Nome: data.name,
                Endereço: data.address,
                "Horário de Funcionamento": {
                    "De segunda à sexta": "Das " + data.opening_hours.weekday.start_time + " às " + data.opening_hours.weekday.final_hour,
                    "Fim de semana": "Das " + data.opening_hours.weekend.start_time + " às " + data.opening_hours.weekend.final_hour
                }
            };

            RouterResponse.success({data: restaurant}, res);
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao buscar o restaurante', res);
        });
        
    }

    /**
     * updateRestaurant
     *
     * Atualiza os dados de um restaurante através do ID
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static restaurant(req: express.Request, res: express.Response){
        let id: any = req.params.id;
        let data: any = req.body;

        const db = firebaseAdmin.firestore().collection('restaurants');

        var restaurant = {
            name: data.name,
            address: data.address,
            opening_hours: {
                weekday: {
                    final_hour: data.weekday.final_hour,
                    start_time: data.weekday.start_time
                },
                weekend: {
                    final_hour: data.weekend.final_hour,
                    start_time: data.weekend.start_time
                }
            }
        };
        if(id){
            db.doc(id).set(restaurant, {merge: true}).then((result) => {
                RouterResponse.success({data: 'Restaurante alterado com sucesso.'}, res);
            }).catch((err) => {
                RouterResponse.error('Houve um erro ao atualizar o restaurante.', res);
            });
        }else{
            db.add(restaurant).then((document) => {
                db.doc(document.id).set({id: document.id}, {merge: true});
                RouterResponse.success({data: 'Restaurante adicionado com sucesso.'}, res);
            }).catch((err) => {
                RouterResponse.error('Houve um erro ao adicionar o restaurante.', res);
            });

        }
        
    }

    /**
     * deleteRestaurant
     *
     * Excluí um restaurante pelo ID
     *
     * @public
     * @author Isabella Garcia
     * @since  10/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static deleteRestaurant(req: express.Request, res: express.Response){
        let id: any = req.params.id;

        const db = firebaseAdmin.firestore();

        db.collection('restaurants').doc(id).delete().then((result) => {
            RouterResponse.success({data: 'Restaurante excluído com sucesso.'}, res);
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao excluir o restaurante.', res);
        });
    }
}