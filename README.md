# Goomer Lista Rango #

Este README é para explicar passo a passo da aplicação. Esta API foi desenvolvida em NodeJS e banco não relacional Firebase.

### Como é composto a API? ###

* Uma pasta 'Controllers' contendo os métodos
* Uma pasta 'Router' com todos os enpoints e arquivo com padrão de respostas
* Uma pasta 'dist' que é gerada quando a API é inicializada
* Demais arquivos de um projeto Node e configurações do Firebase

### Passo a passo para iniciar a API ###

* Clone o repositório na branch Master
* Rode o comando 'npm install'
* Rode o comando 'npm run build' para iniciar
* A API estará na porta 4000, então na URL ficará 'localhost:4000'
* Disponibilizei um arquivo do Postman para facilitar os testes

### Desafios e Problemas ###

* Encontrar uma maneira de tratar os campos de hora/minuto, pois normalmente é mais fácil fazer pelo frontend
* Upload de arquivos, pois em NodeJS não encontrei uma forma de usar o Firebase Storage